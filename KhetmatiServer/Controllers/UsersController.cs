﻿using KhetmatiServer.Data;
using KhetmatiServer.Jwt;
using KhetmatiServer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace KhetmatiServer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly KhetmatiDBContext _context;

        public UsersController(KhetmatiDBContext context)
        {
            _context = context;
        }

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUser(User user)
        {           
            Guid Id = Guid.NewGuid();
            user.Id = Id.ToString();


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return Ok(JwtMaker.GetToken(Id.ToString()));
        }

    }
}