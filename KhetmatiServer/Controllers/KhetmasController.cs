﻿using KhetmatiServer.Data;
using KhetmatiServer.Enums;
using KhetmatiServer.Models;
using KhetmatiServer.Models.Jwt;
using KhetmatiServer.ViewModels;
using KhetmatiServer.Requests.ViewModels;
using KhetmatiServer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KhetmatiServer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class KhetmasController : ControllerBase
    {
        private readonly KhetmatiDBContext _context;

        private readonly KhetmatService khService;
        private readonly AjzaaService ajService;
        private readonly string userId;

        public KhetmasController(KhetmatiDBContext context,
                                 KhetmatService khService, AjzaaService ajService)
        {
            JwtService autService = new JwtService(User);
            var userId = autService.GetClaim(JwtOurClaims.Id);
            this.userId = userId;

            _context = context;
            this.ajService = ajService;
            this.khService = khService;
        }

        [HttpGet("GetMyKhetmat")]
        public async Task<IActionResult> GetMyKhetmat()
        {
            var khetmatList = await khService.GetCreatedKhetmatByUser(userId)
                                        .Select(x => new KhetmaViewModel { Id = x.Id, BeginDate = x.BeginDate, NumberOfJozeAllowedToTaken = x.NumberOfJozeAllowedToTaken, Name = x.Name, EndDate = x.EndDate, UserName = x.User.Name, UserColor = x.User.Color })
                                        .ToListAsync();

         
            foreach (var khetma in khetmatList)
            {
                var khetmaAjzaa = ajService.GetAjzaaOfKhetma(khetma.Id);

                var userAjzaaListViewModel =await khetmaAjzaa.Where(x => x.UserId == userId)
                    .Select(x => new JozeViewModel { Id = x.Id, Number = x.Number, State = x.State, UserColor = x.User.Color, UserName = x.User.Name, IsForYou = true })
                    .ToListAsync();

                var otherAjzaaListViewModel =await khetmaAjzaa.Where(x => x.UserId != userId)
                    .Select(x => new JozeViewModel { Id = x.Id, Number = x.Number, State = x.State, UserColor = x.User.Color, UserName = x.User.Name, IsForYou = false })
                    .ToListAsync();


                khetma.Ajzaa.AddRange(userAjzaaListViewModel);
                khetma.Ajzaa.AddRange(otherAjzaaListViewModel);
            }

            return Ok(khetmatList);
        }


        [HttpGet("GetJoinedKhetmat")]
        public async Task<IActionResult> GetJoinedKhetmat()
        {
            var joinedKhetmat = await khService.GetJoinedKhetmatByUser(userId, ajService)
                               .Select(x => new KhetmaViewModel { Id = x.Id, BeginDate = x.BeginDate, NumberOfJozeAllowedToTaken = x.NumberOfJozeAllowedToTaken, Name = x.Name, EndDate = x.EndDate, UserName = x.User.Name, UserColor = x.User.Color })
                               .ToListAsync(); 


       
            foreach (var khetma in joinedKhetmat)
            {
                var khetmaAjzaa = ajService.GetAjzaaOfKhetma(khetma.Id);

                var userAjzaaListViewModel = await khetmaAjzaa.Where(x => x.UserId == userId)
                    .Select(x => new JozeViewModel { Id = x.Id, Number = x.Number, State = x.State, UserColor = x.User.Color, UserName = x.User.Name, IsForYou = true })
                    .ToListAsync();

                var otherAjzaaListViewModel = await khetmaAjzaa.Where(x => x.UserId != userId)
                    .Select(x => new JozeViewModel { Id = x.Id, Number = x.Number, State = x.State, UserColor = x.User.Color, UserName = x.User.Name, IsForYou = false })
                    .ToListAsync();


                khetma.Ajzaa.AddRange(userAjzaaListViewModel);
                khetma.Ajzaa.AddRange(otherAjzaaListViewModel);
            }

            return Ok(joinedKhetmat);
        }




        //لازم يكون الموديل للي جاية من ال تطبيق الموبايل   
        //KhetmaViewModel
        [HttpPost]
        public async Task<IActionResult> PostKhetma(KhetmaViewModel khetmaViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Khetma _newKhtema = new Khetma();
            _newKhtema.Name = khetmaViewModel.Name;
            _newKhtema.BeginDate = khetmaViewModel.BeginDate;
            _newKhtema.EndDate = khetmaViewModel.EndDate;
            _newKhtema.NumberOfJozeAllowedToTaken = khetmaViewModel.NumberOfJozeAllowedToTaken;
            _newKhtema.UserId = userId;


            await khService.AddKhetma(_newKhtema);      

            return Ok("ok");
        }


        [HttpPost("TakeJoze")]
        public async Task<IActionResult> TakeJoze(JzaaRequestViewModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
 

            var khetma = await khService.GetKhetma(request.KhetmaId).FirstOrDefaultAsync();
            if (khetma == null)
                return BadRequest("ThereIsNoKetmaWithThisId");


            var ajzaaOfUserOfthisKhtma = ajService.GetAjzaaOfKhetma(request.KhetmaId).Where(x=>x.UserId == userId);
            var ajzaaOfUserOfthisKhtmaCount = await ajzaaOfUserOfthisKhtma.CountAsync();

            if (ajzaaOfUserOfthisKhtmaCount != 0)
            {
                var jzaaOfTheSameNumber = await ajzaaOfUserOfthisKhtma.FirstOrDefaultAsync(x => x.Number == request.JzaaNumber);
                if(jzaaOfTheSameNumber != null )
                    return BadRequest("لقد قمت بحجز الجزء من قبل");
            }


            var selectedJzaa = ajService.GetAjzaaOfKhetma(request.KhetmaId).Where(x => x.Number == request.JzaaNumber).FirstOrDefault();
            if (selectedJzaa != null)
                return BadRequest("هذا الجزء محجوز مسبقا الرجاء اخيار غيره");


            if (ajzaaOfUserOfthisKhtmaCount + 1 > khetma.NumberOfJozeAllowedToTaken)
                return BadRequest("لا يمكن أخذ هذا العدد من الأجزاء");


           var addResult = await ajService.AddJzaa(new Joze { KhetmaId =request.KhetmaId, Number = request.JzaaNumber, UserId = userId, State = (int)JozeState.Taken });
            if (!addResult)
                return BadRequest("SomethingWrongPleaseTryAgainLater");



            return Ok("ok");
        }


        [HttpPost("ReadJoze")]
        public async Task<IActionResult> ReadJoze(JzaaRequestViewModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var khetma = await khService.GetKhetma(request.KhetmaId).FirstOrDefaultAsync();
            if (khetma == null)
                return BadRequest("ThereIsNoKetmaWithThisId");




            var ajzaaOfUserOfthisKhtma = ajService.GetAjzaaOfKhetma(request.KhetmaId).Where(x => x.UserId == userId);
            var ajzaaOfUserOfthisKhtmaCount = await ajzaaOfUserOfthisKhtma.CountAsync();


            if (ajzaaOfUserOfthisKhtmaCount == 0)
                return BadRequest("لا يوجد جزء محجوز من قبلك لتجعله مقروء");


            var Jzaa = await ajzaaOfUserOfthisKhtma.Where(x=>x.Number == request.JzaaNumber).FirstOrDefaultAsync();
            if (Jzaa == null)
                return BadRequest("هذا الجزء ليس محجوز من قبلك حتى تعلمه كمقروء");


            Jzaa.State = (int)JozeState.Readed;


            if (await ajService.GetAjzaaOfKhetma(request.KhetmaId).Where(x => x.State == (int)JozeState.Readed).CountAsync() == 29)
            {
                var result = await khService.MarkAsRead(request.KhetmaId);
                if (!result)
                    return BadRequest("SomethinWrong");
            }
       
            await _context.SaveChangesAsync();

            return Ok("ok");
        }


        [HttpPost("LeftJoze")]
        public async Task<IActionResult> LeftJoze(JzaaRequestViewModel request)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);


            var khetma = await khService.GetKhetma(request.KhetmaId).FirstOrDefaultAsync();
            if (khetma == null)
                return BadRequest("ThereIsNoKetmaWithThisId");



            var ajzaaOfUserOfthisKhtma = ajService.GetAjzaaOfKhetma(request.KhetmaId).Where(x => x.UserId == userId);
            var ajzaaOfUserOfthisKhtmaCount = await ajzaaOfUserOfthisKhtma.CountAsync();


            if (ajzaaOfUserOfthisKhtmaCount == 0)
                return BadRequest("لا يوجد جزء محجوز من قبلك لتقوم بحذفه");


            var Jzaa = await ajzaaOfUserOfthisKhtma.Where(x => x.Number == request.JzaaNumber).FirstOrDefaultAsync();
            if (Jzaa == null)
                return BadRequest("هذا الجزء ليس محجوز من قبلك لتقوم بحذفه");


            ajService.RemoveJzaa(Jzaa);

            await _context.SaveChangesAsync();

            return Ok("ok");
        }

    }
}