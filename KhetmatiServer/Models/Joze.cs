﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhetmatiServer.Models
{
    public class Joze
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "الحالة"), Range(1, 3)]
        public byte State { get; set; }

        [Display(Name = "الجزء"), Range(1, 30)]
        public byte Number { get; set; }

        public Khetma Khetma { get; set; }
        [ForeignKey(nameof(Khetma))]
        public int KhetmaId { get; set; }

        public User User { get; set; }
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

    }
}
