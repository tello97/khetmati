﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KhetmatiServer.Models
{
    public class User
    {
        [Key]
        public string Id { get; set; }

        [Required, Display(Name = "الختمة")]
        public string Name { get; set; }

        [Required, Display(Name = "اللون")]
        public string Color { get; set; }

        public string DeviceModel { get; set; }

        public string Manufacturer { get; set; }

        public string Version { get; set; }

        public string Platform { get; set; }

        public string Idiom { get; set; }

        public DateTime CreatedDate { get; set; }

        public ICollection<Khetma> Khetmas { get; set; }
        public ICollection<Joze> Jozes { get; set; }

        public User()
        {
            CreatedDate = DateTime.UtcNow;
        }


    }
}
