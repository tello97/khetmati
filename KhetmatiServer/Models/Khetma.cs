﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KhetmatiServer.Models
{
    public class Khetma
    {
        [Key]
        public int Id { get; set; }

        [Required, Display(Name = "الختمة")]
        public string Name { get; set; }

        [Display(Name = "تاريخ الإنشاء"), DataType(DataType.Date)]
        public DateTime BeginDate { get; set; }

        [Display(Name = "تاريخ الإنتهاء")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "عدد الأجزاء المسموح قرائته للشخص")]
        public byte NumberOfJozeAllowedToTaken { get; set; } = 1;

        public bool Readed { get; set; } = false;

        public User User { get; set; }
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        public ICollection<Joze> Jozes { get; set; }

        public Khetma()
        {
            this.BeginDate = DateTime.UtcNow;
        }
    }
}
