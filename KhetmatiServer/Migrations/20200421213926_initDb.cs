﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KhetmatiServer.Migrations
{
    public partial class initDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Color = table.Column<string>(nullable: false),
                    DeviceModel = table.Column<string>(nullable: true),
                    Manufacturer = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Platform = table.Column<string>(nullable: true),
                    Idiom = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Khetmas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    NumberOfJozeAllowedToTaken = table.Column<byte>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Khetmas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Khetmas_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Jozes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    State = table.Column<byte>(nullable: false),
                    Number = table.Column<byte>(nullable: false),
                    KhetmaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Jozes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Jozes_Khetmas_KhetmaId",
                        column: x => x.KhetmaId,
                        principalTable: "Khetmas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Jozes_KhetmaId",
                table: "Jozes",
                column: "KhetmaId");

            migrationBuilder.CreateIndex(
                name: "IX_Khetmas_UserId",
                table: "Khetmas",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Jozes");

            migrationBuilder.DropTable(
                name: "Khetmas");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
