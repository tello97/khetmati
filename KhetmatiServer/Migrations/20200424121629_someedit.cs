﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KhetmatiServer.Migrations
{
    public partial class someedit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Readed",
                table: "Khetmas",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Readed",
                table: "Khetmas");
        }
    }
}
