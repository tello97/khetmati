﻿// <auto-generated />
using System;
using KhetmatiServer.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace KhetmatiServer.Migrations
{
    [DbContext(typeof(KhetmatiDBContext))]
    [Migration("20200421213926_initDb")]
    partial class initDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.11-servicing-32099")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("KhetmatiServer.Models.Joze", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("KhetmaId");

                    b.Property<byte>("Number");

                    b.Property<byte>("State");

                    b.HasKey("Id");

                    b.HasIndex("KhetmaId");

                    b.ToTable("Jozes");
                });

            modelBuilder.Entity("KhetmatiServer.Models.Khetma", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BeginDate");

                    b.Property<DateTime?>("EndDate");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<byte>("NumberOfJozeAllowedToTaken");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Khetmas");
                });

            modelBuilder.Entity("KhetmatiServer.Models.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Color")
                        .IsRequired();

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("DeviceModel");

                    b.Property<string>("Idiom");

                    b.Property<string>("Manufacturer");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Platform");

                    b.Property<string>("Version");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("KhetmatiServer.Models.Joze", b =>
                {
                    b.HasOne("KhetmatiServer.Models.Khetma", "Khetma")
                        .WithMany("Jozes")
                        .HasForeignKey("KhetmaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("KhetmatiServer.Models.Khetma", b =>
                {
                    b.HasOne("KhetmatiServer.Models.User", "User")
                        .WithMany("Khetmas")
                        .HasForeignKey("UserId");
                });
#pragma warning restore 612, 618
        }
    }
}
