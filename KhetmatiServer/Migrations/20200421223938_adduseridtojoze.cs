﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace KhetmatiServer.Migrations
{
    public partial class adduseridtojoze : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Jozes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Jozes_UserId",
                table: "Jozes",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Jozes_Users_UserId",
                table: "Jozes",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Jozes_Users_UserId",
                table: "Jozes");

            migrationBuilder.DropIndex(
                name: "IX_Jozes_UserId",
                table: "Jozes");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Jozes");
        }
    }
}
