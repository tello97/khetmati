﻿using KhetmatiServer.Models;
using Microsoft.EntityFrameworkCore;

namespace KhetmatiServer.Data
{
    public class KhetmatiDBContext : DbContext
    {

        public KhetmatiDBContext(DbContextOptions<KhetmatiDBContext> options)
          : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Khetma> Khetmas { get; set; }
        public DbSet<Joze> Jozes { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
