﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using KhetmatiServer.Models.Jwt;


namespace KhetmatiServer.Jwt
{
    public static class JwtMaker
    {
        public static string GetToken(string UserId)
        {
            string securitykey = "this_M&M_securitykey!@#$%^";
            var symmetricsecuritykey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securitykey));
            var signincredentials = new SigningCredentials(symmetricsecuritykey, SecurityAlgorithms.HmacSha256Signature);
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtOurClaims.Id, UserId));
            var token = new JwtSecurityToken(

                issuer: "Khetmati_App",
                audience: "Khetmati_App_Users",
                expires: DateTime.Now.AddYears(2),
                claims: claims,
                signingCredentials: signincredentials
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
