﻿using System;
using System.Collections.Generic;

namespace KhetmatiServer.ViewModels
{
    public class KhetmaViewModel
    {
        public int Id { get; set; }
       
        public string Name { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public byte NumberOfJozeAllowedToTaken { get; set; }

        public string UserName { get; set; }

        public string UserColor { get; set; }

        public List<JozeViewModel> Ajzaa { get; set; } = new List<JozeViewModel>();
    }
}
