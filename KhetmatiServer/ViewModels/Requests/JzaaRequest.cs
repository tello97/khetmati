﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace KhetmatiServer.Requests.ViewModels
{
    public class JzaaRequestViewModel
    {
        [Required, Range(1,int.MaxValue)]
        public byte KhetmaId { get; set; }

        [Required, Range(1, 30)]
        public byte JzaaNumber { get; set; }
    }
}
