﻿namespace KhetmatiServer.ViewModels
{
    public class JozeViewModel
    {
        public int Id { get; set; }

        public byte State { get; set; }

        public byte Number { get; set; }

        public string UserName { get; set; }

        public string UserColor { get; set; }

        public bool IsForYou { get; set; } = false;
    }
}
