﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KhetmatiServer.Services
{
    public class JwtService
    {
        ClaimsPrincipal User;

        public JwtService(ClaimsPrincipal User )
        {
            this.User = User;
        }

        public string GetClaim(string claimKey)
        {
            var identity = User.Identity as ClaimsIdentity;
            IEnumerable<Claim> claims = identity.Claims;
            var idclaim = claims.Where(x => x.Type == claimKey).FirstOrDefault();
            return idclaim.Value;
        }
    }
}
