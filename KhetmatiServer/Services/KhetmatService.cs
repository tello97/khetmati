﻿using KhetmatiServer.Data;
using KhetmatiServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace KhetmatiServer.Services
{
    public class KhetmatService
    {

        KhetmatiDBContext _context;

        public KhetmatService([FromServices] KhetmatiDBContext context)
        {
            this._context = context;
        }

        public  IQueryable<Khetma> GetCreatedKhetmatByUser(string userID)
        {            
            var khetmatLst = _context.Khetmas.Where(x => x.UserId == userID);
            return khetmatLst;
        }

        public IQueryable<Khetma> GetJoinedKhetmatByUser(string userID, AjzaaService ajService )
        {
            if (ajService == null)
                return null;

            var khetmatId = ajService.GetUserAjzaa(userID).Select(x => x.KhetmaId).Distinct();

            var khetmatLst = _context.Khetmas.Where(x => khetmatId.Contains(x.Id));

            return khetmatLst;
        }

        public  IQueryable<Khetma> GetKhetma(int khetmaId)
        {
           return  _context.Khetmas.Where(x => x.Id == khetmaId);
        }

        public async  Task<bool> AddKhetma(Khetma kh)
        {
            await _context.Khetmas.AddAsync(kh);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> MarkAsRead(int khetmaId)
        {
            var khetma =  await _context.Khetmas.FirstOrDefaultAsync(x => x.Id == khetmaId);
            if (khetma == null)
                return false;

            khetma.Readed = true;

            await _context.SaveChangesAsync();

            return true;
        }

    }
}
