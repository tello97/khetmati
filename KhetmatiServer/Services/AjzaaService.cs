﻿using KhetmatiServer.Data;
using KhetmatiServer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;



namespace KhetmatiServer.Services
{
    public class AjzaaService 
    {

        KhetmatiDBContext _context;

        public AjzaaService( [FromServices] KhetmatiDBContext context)
        {
            this._context = context;
        }

        public IQueryable<Joze> GetUserAjzaa(string userID)
        {
            var ajzaaLst = _context.Jozes.Where(x => x.UserId == userID);
            return ajzaaLst;
        }

        public IQueryable<Joze> GetAjzaaOfKhetma(int khetmaId)
        {
            var ajzaaLst = _context.Jozes.Where(x => x.KhetmaId == khetmaId);
            return ajzaaLst;
        }

        public async  Task<bool> AddJzaa(Joze _jzaa)
        {
            if (_jzaa == null)
                return false;

            await _context.Jozes.AddAsync(_jzaa);
            return true;
        }

        public bool RemoveJzaa(Joze _jzaa)
        {
            if (_jzaa == null)
                return false;

            _context.Jozes.Remove(_jzaa);
            return true;
        }


    }
}
